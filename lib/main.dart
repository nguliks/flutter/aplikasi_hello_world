import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Aplikasi Hello World'),
        ),
        body: Center(
            child: Container(
                color: Colors.lightBlue,
                width: 200,
                height: 100,
                child: Text(
                  'Hello World, hallo nama saya Muhammad Aria Muktadir, saya akan membuat aplikasi flutter',
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.white,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.bold,
                  ),
                ))),
      ),
    );
  }
}
